#!/bin/sh

set -ex

if [ -z "$VK_DRIVER" ]; then
   echo 'VK_DRIVER must be to something like "radeon" or "intel" for the test run'
   exit 1
fi

INSTALL=`pwd`/data/install

# Set up the driver environment.
export LD_LIBRARY_PATH=`pwd`/data/install/lib/
export VK_ICD_FILENAMES=`pwd`/data/install/share/vulkan/icd.d/"$VK_DRIVER"_icd.x86_64.json

# To store Fossilize logs on failure.
RESULTS=`pwd`/results
mkdir -p results

"$INSTALL/fossils/fossils.sh" "$INSTALL/fossils.yml" "$RESULTS"
